package main

import (
	"log"
	"os"
	"time"

	"gopkg.in/alecthomas/kingpin.v2"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

var (
	logFlag = log.LUTC | log.Ldate
	broker  = kingpin.Flag("broker", "MQTT broker to send messages to").Default("").OverrideDefaultFromEnvar("MQTT_BROKER").
		Short('b').Required().URL()
	clientID = kingpin.Flag("clientID", "ID to use when connecting to the broker").
			Default("demo-device").OverrideDefaultFromEnvar("MQTT_CLIENT_ID").Short('c').String()
)

func main() {

	log.SetFlags(logFlag)
	log.SetPrefix("[mqtt-gen  ] ")
	log.SetOutput(os.Stderr)
	log.Println("Starting up...")

	kingpin.Version("0.0.1-SNAPSHOT")

	log.Println("Parsing flags")
	kingpin.Parse()
	log.Println("Parsed Flags")

	log.Println("Setting up Eclipse PAHO client...")
	mqtt.DEBUG = log.New(os.Stderr, "[paho-debug] ", logFlag)
	mqtt.ERROR = log.New(os.Stderr, "[paho-error] ", logFlag)

	opts := mqtt.NewClientOptions()
	opts.AddBroker((*broker).String())
	opts.SetClientID(*clientID)
	opts.SetKeepAlive(2 * time.Second)
	opts.SetPingTimeout(1 * time.Second)

	c := mqtt.NewClient(opts)
	// c.Connect()
	// if !c.IsConnected() {
	// 	log.Println("PAHO not connected - ")
	// }
	log.Println("Eclipse PAHO set up successfully!")

	tick := time.NewTicker(3 * time.Second)

	for {
		select {
		case t := <-tick.C:
			c.Publish(
				"beispielkunde/beispielnetz/rack1/slot4",
				2, false,
				t)
		}
	}
}
